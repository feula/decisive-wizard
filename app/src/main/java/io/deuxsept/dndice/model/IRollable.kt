package io.deuxsept.dndice.model

/**
 * Created by Flo on 13/08/2016.
 */
interface IRollable {
    fun roll(): List<Int>
    fun resultsAsString(): String

    var results: MutableList<Int>
}