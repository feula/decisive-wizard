package io.deuxsept.dndice.model

import java.util.*

/**
 * Represents a single roll, with dice type, amount of rolls and added bonus
 * Use this as if it were an immutable class until further Kotlin learning is done.
 * Do. Not. Modify. The. Fields. Of. An. Existing. Instance.
 */
class DiceRoll
/**
 * If you're using this constructor for anything else than tests, you're stupid. Use from_string
 */(data: List<IRollable>) {
    private var elementsInRoll: MutableList<IRollable> = mutableListOf()

    /**
     * Override equality comparison
     * Two rolls are the same when their bonus, dice type and rollcount match
     */
    override fun equals(other: Any?): Boolean {
        return when(other) {
            is DiceRoll -> {
                other.elementsInRoll.size == this.elementsInRoll.size && other.elementsInRoll.none {
                    it -> !this.elementsInRoll.contains(it) }
            }
            else -> { false }
        }
    }

    /**
     * Override hashcode implementation
     */
    override fun hashCode(): Int {
        return Objects.hash(this.elementsInRoll)
    }

    /**
     * Returns this roll as a liste of elements separated by '+'
     */
    override fun toString(): String {
        return elementsInRoll.joinToString(" + ")
    }

    /**
     * Returns the formula used for the roll, with dices first then bonuses and appropriate signs.
     */
    fun formula(): String {
        val prefixed = StringBuilder()
        elementsInRoll.forEach {
            item -> run {
                prefixed.append(when(item) {
                    is Dice -> (if (item.dice_rolls > 0) "+" else "") + "${item.dice_rolls}d${item.dice_type}"
                    is FlatBonus -> (if (item.bonus > 0) "+" else "") + "${item.bonus}"
                    else -> "panicpanicpanicpanic"
                })
            }
        }

        return prefixed.removePrefix("+").toString()
    }

    /**
     * Get random results for this dice roll
     * Returns a list of random values as well as the flat bonuses
     */
    fun roll(): DiceRollResult {
        val rolls: MutableList<IRollable> = mutableListOf()

        elementsInRoll.forEach {
            item -> run { item.roll(); rolls.add(item) }
        }

        return DiceRollResult(rolls, formula())
    }

    /**
     * Utilities to create a new dice roll
     */
    companion object DiceCreator {
        /**
         * Create a new DiceRoll from a String value
         * Will ignore any and all spaces
         * ex: DiceRoll.from_string("4d20 + 12")
         */
        fun fromString(value: String): DiceRoll {
            val accumulator = StringBuilder()
            val elements: MutableList<IRollable> = mutableListOf()
            var charsParsed = 0
            var triggerParse = false
            val stripped = value.replace(" ", "")

            stripped.forEach {
                char -> run {
                    if (char == '+' || char == '-') {
                        triggerParse = true
                    }
                    // If we reach the last character, add it to parse
                    if (charsParsed+1 == stripped.length && !(char == '+' || char == '-')) {
                        accumulator.append(char)
                        triggerParse = true
                    }

                    if (triggerParse) {
                        val parsed: IRollable = if (accumulator.contains('d')) interpretDice(accumulator.toString()) else interpretBonus(accumulator.toString())

                        // Only add the rolls if they're non zero
                        when(parsed) {
                            is FlatBonus -> if(parsed.bonus != 0) elements.add(parsed)
                            is Dice      -> if(parsed.dice_rolls != 0) elements.add(parsed)
                        }
                        triggerParse = false
                        accumulator.setLength(0)
                    }

                    accumulator.append(char)
                    charsParsed++
                }
            }

            return DiceRoll(elements)
        }

        /**
         * Interpret <value> as a dice
         * ex: "2d10" will return a Dice(2, 10)
         */
        private fun interpretDice(value: String): Dice {
            val splitted = value.split('d')
            // 'd100' is a perfectly valid value, we just reinterpret that as 1d100
            val rolls = when(splitted[0].length) {
                0 -> { 1 }
                1 -> {
                    when {
                        splitted[0] == "-" -> -1
                        splitted[0] == "+" -> 1
                        else -> splitted[0].toInt()
                    }
                }
                else -> { splitted[0].toInt() }
            }
            val max = when(splitted.size) {
                1 -> { splitted[0].toInt() }
                else -> { splitted[1].toInt() }
            }
            return Dice(rolls , max)
        }

        /**
         * Interpret <value> as a bonus
         */
        private fun interpretBonus(value: String): FlatBonus {
            return when(value.length) {
                1 -> if (value[0] == '+' || value[0] == '-') FlatBonus(0) else FlatBonus(value.toInt())
                else -> FlatBonus(value.toInt())
            }
        }
    }

    init {
        elementsInRoll = data.toMutableList()
    }
}