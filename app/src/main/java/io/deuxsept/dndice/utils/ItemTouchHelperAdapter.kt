package io.deuxsept.dndice.utils

/**
 * Created by Luo
 * 15/08/2016.
 */
interface ItemTouchHelperAdapter {
    fun onItemMove(fromPosition: Int, toPosition: Int)
    fun onItemMoved(fromPosition: Int, toPosition: Int)
}