package io.deuxsept.dndice.main

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.deuxsept.dndice.adapter.RecentAdapter
import io.deuxsept.dndice.database.DatabaseHelper
import io.deuxsept.dndice.R
import kotlinx.android.synthetic.main.fragment_recent.*

/**
 * Created by Luo
 * 13/08/2016.
 */
class RecentFragment : Fragment() {

    private lateinit var mDb: DatabaseHelper
    private lateinit var mAdapter: RecentAdapter

    companion object {
        private var mFragment: RecentFragment? = null

        fun newInstance(): RecentFragment {
            if (mFragment == null) {
                mFragment = RecentFragment()
            }
            return mFragment as RecentFragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mDb = DatabaseHelper.getInstance(context!!)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_recent, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recent_recycler.layoutManager = layoutManager
        mAdapter = RecentAdapter(this)
        recent_recycler.adapter = mAdapter

        getRollsFromDatabase()
    }

    private fun getRollsFromDatabase() {
        val rolls = mDb.getAllRecentRolls()
        if (rolls.count() != 0)
            mAdapter.addAll(rolls)
        shouldShowEmptyState()
    }

    private fun shouldShowEmptyState() {
        if (mAdapter.itemCount == 0) recent_none_view.visibility = View.VISIBLE
        else recent_none_view.visibility = View.GONE
    }
}