package io.deuxsept.dndice.main

import android.animation.Animator
import android.animation.ObjectAnimator
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.PopupMenu
import android.util.DisplayMetrics
import android.view.*
import android.view.animation.*
import android.widget.EditText
import android.widget.TextView
import io.deuxsept.dndice.database.DatabaseHelper
import io.deuxsept.dndice.model.DiceRoll
import io.deuxsept.dndice.model.Preferences
import io.deuxsept.dndice.model.RollModel
import io.deuxsept.dndice.R
import io.deuxsept.dndice.utils.Utils
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.dialog_add_favorite.*
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*

class HomeFragment : android.support.v4.app.Fragment() {

    private var mWidth: Int = 0
    private var mHeight: Int = 0

    private lateinit var mDb: DatabaseHelper

    private var mDataStack: Stack<String> = Stack()
    private var mDisplayWarningShowed: Boolean = false
    var mResultViewOpened: Boolean = false

    companion object {
        private var mFragment: HomeFragment? = null

        fun newInstance(): HomeFragment {
            if (mFragment == null) {
                mFragment = HomeFragment()
            }
            return mFragment as HomeFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDb = DatabaseHelper.getInstance(context!!)!!

        val metrics = DisplayMetrics()
        val wm = context!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.defaultDisplay.getMetrics(metrics)
        mWidth = metrics.widthPixels
        mHeight = metrics.heightPixels
    }

    override fun onResume() {
        super.onResume()
        refreshFormula()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initEventHandlers()
    }

    /*
     * Sets up the various even handlers for the UI
     */
    private fun initEventHandlers() {
        // Delete one element on single backspace tap
        backspace_button.setOnClickListener {
            if (mDataStack.size > 0) {
                mDataStack.pop()
                refreshFormula()
            }
        }
        // Delete entire formula on long backspace press
        backspace_button.setOnLongClickListener {
            mDataStack.clear()
            refreshFormula()
            true
        }

        activity?.last_roll_text_view?.setOnClickListener {
            if (activity!!.last_roll_text_view.text != "" && !mResultViewOpened) {
                openResultView()
            }
        }

        // Fill the stack with appropriate data on favorite select
        fav_button.setOnClickListener {
            val menu = PopupMenu(context!!, fav_button)
            // Fill the formula textview
            menu.setOnMenuItemClickListener { item ->
                mDataStack.clear()
                // Fun? This makes it so the entire favourite is deleted at once because it is one
                // element in the stack... weird feature?
                pushWithAutoSymbols(mDb.getFavorite(item.itemId)?.formula)
                if (Preferences.getInstance(context!!).QuickFavouritesRoll) {
                    executeRoll()
                }
                refreshFormula()
                true
            }
            // Fill the favourite menu
            for ((formula, _, _, name, id) in mDb.getAllFavoritesRolls()) {
                menu.menu.add(0, id, Menu.NONE, "$name ($formula)")
            }
            menu.show()
        }

        // Roll the dices on tap
        roll_button.setOnClickListener {
            if (mDataStack.size > 0) {
                executeRoll()
                if (!Preferences.getInstance(context!!).HideRollWindow) {
                    openResultView()
                }
            } else {
                showDisplayWarning()
            }
        }

        fav_result_fab.setOnClickListener {
            favoriteRoll()
        }
        close_result_fab.setOnClickListener {
            closeResultView()
        }
        replay_result_fab.setOnClickListener {
            executeRoll()
        }
    }

    /**
     * UI Modifications
     */
    private fun showDisplayWarning() {
        mDisplayWarningShowed = true
        display_empty_warning.animate()
                .setDuration(300)
                .alpha(1f)
                .setInterpolator(AccelerateInterpolator())
                .withEndAction {
                    val blinkAnimation = AlphaAnimation(1f, 0f)
                    blinkAnimation.duration = 300
                    blinkAnimation.interpolator = LinearInterpolator()
                    blinkAnimation.repeatCount = 3
                    blinkAnimation.repeatMode = Animation.REVERSE
                    display_empty_warning.startAnimation(blinkAnimation)
                }.start()
    }

    private fun hideDisplayWarning() {
        mDisplayWarningShowed = false
        display_empty_warning.animate()
                .setDuration(300)
                .alpha(0f)
                .setInterpolator(AccelerateInterpolator()).start()
    }

    private fun openResultView() {
        Utils.circularReveal(result_view, mWidth /2, mHeight)
        fav_result_fab.show()
        close_result_fab.show()
        replay_result_fab.show()
        mResultViewOpened = true
    }

    fun closeResultView() {
        raise()
        mDataStack.clear()
        refreshFormula()
        mResultViewOpened = false
    }

    private fun raise() {
        val objectAnimator = ObjectAnimator.ofInt(result_view, "bottom",
                result_view.bottom, result_view.top)
        objectAnimator.interpolator = AccelerateDecelerateInterpolator()
        objectAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) { }
            override fun onAnimationCancel(p0: Animator?) { }
            override fun onAnimationStart(animation: Animator) { }
            override fun onAnimationEnd(animation: Animator) {
                result_view.visibility = View.GONE
            }
        })
        objectAnimator.start()
    }

    /**
     * Data Operations
     */
    private fun executeRoll() {
        val dice: DiceRoll = DiceRoll.fromString(mDataStack.joinToString(""))
        val result = dice.roll()
        formula_var.text = dice.formula()
        detail_var.text = result.as_readable_string()
        result_var.text = result.as_total_string()
        (activity as MainActivity).setLastRoll(result.as_total(), result.roll_info())

        if (!result.as_readable_string().equals("") && !result.as_total().toString().equals("")) {
            mDb.addRecentRoll(RollModel(
                    dice.formula(),
                    result.as_total_string(),
                    result.as_readable_string()
            ))
        }

    }

    private fun favoriteRoll() {
        val dialog = Dialog(context)
        dialog.setContentView(R.layout.dialog_add_favorite)
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        dialog.show()
        val name: EditText = dialog.findViewById(R.id.roll_name) as EditText
        name.setText(formula_var.text.toString(), TextView.BufferType.EDITABLE)
        name.selectAll()
        dialog.add_roll_button.setOnClickListener {
            mDb.addFavoriteRoll(RollModel(formula_var.text.toString(),"","",name.text.toString()))
            dialog.dismiss()
        }
    }

    fun pushElementToStack(view: View) {
        if (mDisplayWarningShowed)
            hideDisplayWarning()

        // Check if we're not trying to add too long of a number, without preventing from adding a +/-
        if (view.id != R.id.button_moins && view.id != R.id.button_plus)
            if (mDataStack.size - mDataStack.lastIndexOf("+") > 4 &&
                    mDataStack.size - mDataStack.lastIndexOf("-") > 4)
                return

        pushWithAutoSymbols(when(view.id) {
            R.id.button_0 -> "0"
            R.id.button_1 -> "1"
            R.id.button_2 -> "2"
            R.id.button_3 -> "3"
            R.id.button_4 -> "4"
            R.id.button_5 -> "5"
            R.id.button_6 -> "6"
            R.id.button_7 -> "7"
            R.id.button_8 -> "8"
            R.id.button_9 -> "9"
            R.id.button_plus -> "+"
            R.id.button_moins -> "-"
            R.id.d2 -> "d2"
            R.id.d3 -> "d3"
            R.id.d4 -> "d4"
            R.id.d6 -> "d6"
            R.id.d8 -> "d8"
            R.id.d10 -> "d10"
            R.id.d12 -> "d12"
            R.id.d20 -> "d20"
            R.id.d100 -> "d100"
            else -> ""
        })

        refreshFormula()
    }

    private fun pushWithAutoSymbols(value: String?) {
        if (value == null) return
        if (mDataStack.size > 0 && mDataStack.peek().contains('d') && value != "+" && value != "-") {
            mDataStack.push("+")
        }
        mDataStack.push(value)
    }

    private fun refreshFormula() {
        var formula = ""
        mDataStack.forEach {
            item -> formula += if (item == "+" || item == "-") " $item " else item
        }
        display.text = formula
    }
}
