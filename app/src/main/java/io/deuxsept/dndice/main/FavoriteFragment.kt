package io.deuxsept.dndice.main

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.deuxsept.dndice.adapter.FavoriteAdapter
import io.deuxsept.dndice.database.DatabaseHelper
import io.deuxsept.dndice.model.RollModel
import io.deuxsept.dndice.R
import io.deuxsept.dndice.utils.SimpleItemTouchHelperCallback
import io.deuxsept.dndice.utils.SwipeableRecyclerViewTouchListener
import kotlinx.android.synthetic.main.fragment_favorite.*

/**
 * Created by Luo
 * on 15/08/2016.
 */
class FavoriteFragment : Fragment(), FavoriteAdapter.OnDragStartListener {

    lateinit var mDb: DatabaseHelper
    lateinit var mAdapter: FavoriteAdapter
    private lateinit var mItemTouchHelper: ItemTouchHelper

    companion object {
        private var mFragment: FavoriteFragment? = null

        fun newInstance(): FavoriteFragment {
            if (mFragment == null) {
                mFragment = FavoriteFragment()
            }
            return mFragment as FavoriteFragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mDb = DatabaseHelper.getInstance(context!!)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        favorite_recycler.layoutManager = layoutManager
        mAdapter = FavoriteAdapter(this, this)
        favorite_recycler.adapter = mAdapter

        val callback: ItemTouchHelper.Callback = SimpleItemTouchHelperCallback(mAdapter)
        mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper.attachToRecyclerView(favorite_recycler)

        val swipeTouchListener = SwipeableRecyclerViewTouchListener(favorite_recycler, R.id.foreground, R.id.background,
                object : SwipeableRecyclerViewTouchListener.SwipeListener {
                    override fun onDismissedBySwipe(recyclerView: RecyclerView, reverseSortedPositions: IntArray) {
                        for (position in reverseSortedPositions) {
                            val model: RollModel = mAdapter.getItem(position)
                            mDb.deleteFavoriteRoll(model.id)
                            mAdapter.removeItem(position)
                            shouldShowEmptyState()
                            val snackbar = Snackbar.make(view, "Favorite deleted.", Snackbar.LENGTH_LONG)
                                    .setAction("UNDO") {
                                        mDb.addFavoriteRoll(model)
                                        mAdapter.addItem(position, model)
                                        shouldShowEmptyState()
                                    }
                            snackbar.setActionTextColor(ContextCompat.getColor(context!!, R.color.colorAccent))
                            snackbar.show()
                        }
                        mAdapter.notifyDataSetChanged()
                    }

                    override fun canSwipe(position: Int): Boolean {
                        return true
                    }
                })
        favorite_recycler.addOnItemTouchListener(swipeTouchListener)

        getRollsFromDatabase()
    }

    private fun getRollsFromDatabase() {
        val rolls = mDb.getAllFavoritesRolls()
        if (rolls.count() != 0)
            mAdapter.addAll(rolls)
        shouldShowEmptyState()
    }

    fun shouldShowEmptyState() {
        if (mAdapter.itemCount == 0) favorite_none_view.visibility = View.VISIBLE
        else favorite_none_view.visibility = View.GONE
    }

    override fun onDragStarted(viewHolder: RecyclerView.ViewHolder) {
        mItemTouchHelper.startDrag(viewHolder)
    }
}