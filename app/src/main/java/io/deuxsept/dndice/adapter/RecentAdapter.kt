package io.deuxsept.dndice.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.PorterDuff
import android.os.Build
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import io.deuxsept.dndice.database.DatabaseHelper
import io.deuxsept.dndice.main.RecentFragment
import io.deuxsept.dndice.model.RollModel
import io.deuxsept.dndice.R
import kotlinx.android.synthetic.main.dialog_add_favorite.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Luo
 * 13/08/2016.
 */
class RecentAdapter @Suppress("DEPRECATION") constructor(fragment: RecentFragment) :
        RecyclerView.Adapter<RecentAdapter.ViewHolder>() {

    private var mFragment: RecentFragment = fragment
    private val mList = ArrayList<RollModel>()
    private var lastPosition = -1
    private var mLocale: Locale
    private var mDb: DatabaseHelper
    private var _ctx: Context
    private var _activity: Activity

    init {
        if (fragment.context == null) throw IllegalStateException("Fragment must have a context")
        mLocale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            fragment.context!!.resources.configuration.locales.get(0)
        else
            fragment.context!!.resources.configuration.locale
        mDb = DatabaseHelper(fragment.context!!)
        _ctx = fragment.context!!
        _activity = fragment.activity!!
    }

    companion object {
        var mListener: ViewHolder.onItemClick? = null
    }

    class ViewHolder(itemView: View, listener: ViewHolder.onItemClick) :
            RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var mLayout: View
        internal var mResult: TextView
        internal var mFormula: TextView
        internal var mDetail: TextView
        internal var mTimestamp: TextView
        internal var mFavButton: ImageView

        init {
            mListener = listener
            mLayout = itemView.findViewById(R.id.row_layout)
            mResult = itemView.findViewById(R.id.recent_result) as TextView
            mFormula = itemView.findViewById(R.id.recent_formula) as TextView
            mDetail = itemView.findViewById(R.id.recent_detail) as TextView
            mTimestamp = itemView.findViewById(R.id.recent_timestamp) as TextView
            mFavButton = itemView.findViewById(R.id.recent_fav_button) as ImageView
            mFavButton.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            mListener!!.onClick(v, adapterPosition)
        }

        internal fun clearAnimation() {
            mLayout.clearAnimation()
        }

        interface onItemClick {
            fun onClick(caller: View, position: Int)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentAdapter.ViewHolder {
        val itemView = LayoutInflater.from(mFragment.context).inflate(R.layout.row_recent, parent, false)
        return ViewHolder(itemView, object : ViewHolder.onItemClick {
            override fun onClick(caller: View, position: Int) {
                val model: RollModel = mList[position]
                // If we're adding, first check for no duplication (and ask)
                // Then ask for name and etc.
                val alert = AlertDialog.Builder(_activity)
                    .setTitle("Confirm duplication")
                    .setMessage("You already have a favourite with the same dice formula. Create anyways ?")
                    .setNegativeButton(android.R.string.no, { _, _ -> })
                    .setPositiveButton(android.R.string.yes, { _, _ ->
                        favoriteRoll(model)
                    })
                    .create()

                if (mDb.getAllFavoritesRolls().any { it -> it.formula == model.formula })
                    alert.show()
                else
                    favoriteRoll(model)
            }
        })
    }

    private fun favoriteRoll(model: RollModel) {
        val dialogFavoriteAdd = Dialog(_ctx)
        dialogFavoriteAdd.setContentView(R.layout.dialog_add_favorite)
        dialogFavoriteAdd.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        dialogFavoriteAdd.show()
        dialogFavoriteAdd.add_roll_button.setOnClickListener {
            val name = dialogFavoriteAdd.findViewById(R.id.roll_name) as EditText
            model.name = name.text.toString()
            dialogFavoriteAdd.dismiss()
            applyAndPropagateFavorite(model)
        }
    }

    private fun applyAndPropagateFavorite(model: RollModel) {
        var modificationDone = false
        for ((i, m) in mList.withIndex()) {
            if (m.formula == model.formula) {
                if (!modificationDone && !m.fav) {
                    m.name = model.name
                    mDb.addFavoriteRoll(m)
                    modificationDone = true
                } else if (!modificationDone && m.fav){
                    mDb.deleteFavoriteRoll(m.id)
                    modificationDone = true
                }
                m.fav = !m.fav
                notifyItemChanged(i)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model: RollModel = mList[position]
        holder.mResult.text = model.result
        holder.mFormula.text = model.formula
        holder.mDetail.text = model.detail
        if (model.fav)  {
            holder.mFavButton.setImageResource(R.drawable.ic_favorite_24dp)
            holder.mFavButton.setColorFilter(ContextCompat.getColor(mFragment.context!!, R.color.colorAccent), PorterDuff.Mode.SRC_IN)
        } else {
            holder.mFavButton.setImageResource(R.drawable.ic_favorite_outlined_24dp)
            holder.mFavButton.setColorFilter(ContextCompat.getColor(mFragment.context!!, R.color.md_grey_500), PorterDuff.Mode.SRC_IN)
        }
        holder.mTimestamp.text = SimpleDateFormat("dd MMM - HH:mm", mLocale).format(Date(model.timestamp))
        setAnimation(holder.mLayout, position)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(mFragment.context, R.anim.item_slide_in_from_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        holder.clearAnimation()
    }

    fun addAll(models: List<RollModel>) {
        mList.addAll(models)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mList.count()
    }
}